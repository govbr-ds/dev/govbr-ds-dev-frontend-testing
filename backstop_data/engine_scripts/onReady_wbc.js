module.exports = async (page, scenario, vp) => {
  const getFirstWord = (string) => {
    const name = string.split(" ");
    return name[0];
  };

  const wbc = getFirstWord(scenario.label);
  const handle = await page.evaluateHandle(
    `document.querySelector("#app > br-tab").shadowRoot.querySelector("div > nav > ul > li.tab-item > button[data-panel='br-${wbc}']")`
  );
  handle.click();
  await page.waitForTimeout(1000);
};
