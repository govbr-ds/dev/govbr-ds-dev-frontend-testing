//const BACKSTOP_TEST_CSS_OVERRIDE = `*{ color: red !important; }`;

const styleContent = `
* {   caret-color: transparent !important; }

.br-menu,
.br-wizard,
.br-wizard[vertical] .wizard-progress {
  height: 720px !important;
}
`;

module.exports = async function (page, scenario) {
  // inject arbitrary css to override styles
  /*page.evaluate(`window._styleData = '${styleContent}'`);
  page.evaluate(() => {
    const style = document.createElement('style');
    style.type = 'text/css';
    const styleNode = document.createTextNode(window._styleData);
    style.appendChild(styleNode);
    document.head.appendChild(style);
  });
*/
await page.addStyleTag({ content: styleContent });

//console.log('BACKSTOP_TEST_CSS_OVERRIDE injected for: ' + scenario.label);
};
