module.exports = async (page, scenario, vp) => {
  await require("./loadCookies")(page, scenario);
  await require("./overrideCSS")(page, scenario);
};

module.exports = async (page, scenario, vp) => {
  const ignoredMessages = [
    "Run `$ backstop test` to generate diff report.",
    "Browser Console Log 0: JSHandle:BackstopTools have been installed.",
    "Browser Console Log 0: JSHandle:Hotjar not launching due to suspicious userAgent:",
    "Browser Console Log 1: JSHandle:Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/80.0.3987.0 Safari/537.36",
    "Browser Console Log 0: JSHandle:ERROR",
    "Browser Console Log 1: JSHandle@error",
    "Browser Console Log 0: JSHandle:Popper: CSS \"margin\" styles cannot be used to apply padding between the popper and its reference element or boundary. To replicate margin, use the `offset` modifier, as well as the `padding` option in the `preventOverflow` and `flip` modifiers."
  ];

  console.log = (message) => {
    ignoredMessages.some((ignore) => message.includes(ignore)) ?
      undefined :
      process.stdout.write(`${message}\n`);
  };

  if (vp.label === 'ZOOM 200%') {
    await page.setViewport({
      "width": 1440/2,
      "height": 900/2,
      deviceScaleFactor: 2,
    });
  }

  if (vp.label === 'ZOOM 300%') {
    await page.setViewport({
      "width": 1440/3,
      "height": 900/3,
      deviceScaleFactor: 3,
    });
  }
};