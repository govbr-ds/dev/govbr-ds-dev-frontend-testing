module.exports = async (page, scenario, vp) => {
    await require("./loadCookies")(page, scenario);

    if (vp.label === 'phone') {
        await page.setViewport({
            width: 640,
            height: 480,
            deviceScaleFactor: 3,
        });
    }

};