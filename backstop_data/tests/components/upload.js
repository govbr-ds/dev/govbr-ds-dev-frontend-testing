const componentUrl = "assets/design-system/dist/components/upload/examples.html";

module.exports = options => {
    return {
        scenarios: [{
            label: "Upload",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
        },
        {
            label: "Upload - Click",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            clickSelector: ".br-upload > .br-button.upload-button"
        }
    ]
    }
}