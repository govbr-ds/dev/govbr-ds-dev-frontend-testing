const componentUrl = "assets/design-system/dist/components/tab/examples.html";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: "Tabs",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
      },
      // {
      //   label: "Tabs - Click Last Child",
      //   url: `${options.baseUrl}/${componentUrl}`,
      //   referenceUrl: `${options.refUrl}/${componentUrl}`,
      //   clickSelector: ".tab-item:last-child",
      // },
      // {
      //   label: "Tabs - Click with counter",
      //   url: `${options.baseUrl}/${componentUrl}`,
      //   referenceUrl: `${options.refUrl}/${componentUrl}`,
      //   readySelector: "[data-counter]",
      //   clickSelector: ".br-tabs[data-counter=true] > .tab-nav > ul > .tab-item:nth-child(2)",
      // },
      // {
      //   label: "Tabs - Hover with counter",
      //   url: `${options.baseUrl}/${componentUrl}`,
      //   referenceUrl: `${options.refUrl}/${componentUrl}`,
      //   readySelector: "[data-counter]",
      //   hoverSelector: ".br-tabs[data-counter=true] > .tab-nav > ul > .tab-item:nth-child(2)",
      // }
    ],
  };
};