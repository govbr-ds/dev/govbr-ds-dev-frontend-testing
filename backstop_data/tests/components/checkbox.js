const componentUrl = "assets/design-system/dist/components/checkbox/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Checkbox",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            {
                "label": "Checkbox - Click",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "clickSelector": ".br-checkbox input[type='checkbox']"
            },
            {
                "label": "Checkbox - Hover",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "hoverSelector": ".br-checkbox input[type='checkbox']"
            },
            {
                "label": "Checkbox - Message",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "hoverSelector": ".feedback"
            }

        ]
    }
}