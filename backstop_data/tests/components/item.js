const componentUrl = "assets/design-system/dist/components/item/examples.html";

module.exports = options => {
    return {
        "scenarios": [
            {
                label: "Item",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hideSelectors: ["img.rounded"],
            },
            {
                label: "Item - Click",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hideSelectors: ["img.rounded"],
                clickSelector: ".br-item .row",
            },
            {
                label: "Item - Hover",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hoverSelector: ".br-item .row",
                hideSelectors: ["img.rounded"],
            },
            {
                label: "Item - Click Checkbox",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hideSelectors: ["img.rounded"],
                readySelector: "[data-toggle=selection]",
                clickSelector: ".br-item .br-checkbox",
            },
            {
                label: "Item - Click Radio",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                hideSelectors: ["img.rounded"],
                readySelector: "[data-toggle=selection]",
                clickSelector: ".br-item .br-radio",
            }
        ]
    }
}