const componentUrl = "assets/design-system/dist/components/header/examples.html";

module.exports = options => {
    
    return {
        "scenarios": [
        {
            label: "Header Padrão",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            selectors: [".br-header"],
        },
        {
            label: "Header - ClickLogin",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            selectors: [".br-header"],
            clickSelectors: [".header-actions .br-sign-in"],
        },
        {
            label: "Header - ClickLoginNotification",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            selectors: [".br-header"],
            clickSelectors: [".header-actions .br-sign-in", ".header-avatar .avatar .br-button.circle"],
        },
        {
            label: "Header - ClickMenu",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            selectors: [".br-header"],
            clickSelectors: [".header-menu-trigger .br-button.circle"],
            viewports:[
                {
                    "label": "Smartphone Portrait",
                    "width": 574,
                    "height": 760,
                },
                {
                    "label": "Smartphone Landscape",
                    "width": 991,
                    "height": 480
                },
                {
                    "label": "Tablet Landscape",
                    "width": 1279,
                    "height": 768
                },
                {
                    "label": "Desktop",
                    "width": 1440,
                    "height": 900,
                },
                {
                    "label": "TV",
                    "width": 1600,
                    "height": 900
                },
            ]
        },
        {
            label: "Header - ClickInput",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            selectors: [".br-header"],
            delay: options.DEFAULT_DELAY,
            clickSelector: ".br-input input",
            viewports: [
                {
                    "label": "Desktop",
                    "width": 1440,
                    "height": 900,
                },
                {
                    "label": "TV",
                    "width": 1600,
                    "height": 900
                },
            ]
        },
        {
            label: "Header - HoverItem",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            selectors: [".br-header"],
            delay: options.DEFAULT_DELAY,
            hoverSelectors: [".br-header", ".header-links", ".br-list", ".br-item"],
            viewports: [
                {
                    "label": "Desktop",
                    "width": 1440,
                    "height": 900,
                },
                {
                    "label": "TV",
                    "width": 1600,
                    "height": 900
                },
            ]
        },
        {
            label: "Header - ClickAcessoRápido",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            clickSelector: ".br-header .header-links .br-button.circle",
            viewports: [
                {
                    label: "Smartphone Landscape",
                    width: 991,
                    height: 480,
                },
                {
                    label: "Tablet Landscape",
                    width: 1279,
                    height: 768,
                },
                {
                    label: "ZOOM 200%",
                    width: 1440,
                    height: 900,
                },
            ],
        },
        {
            label: "Header - ClickBusca",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            clickSelector: [".br-header .header-search-trigger .br-button.circle"],
            viewports: [
                {
                    label: "Smartphone Landscape",
                    width: 991,
                    height: 480,
                },
                {
                    label: "Tablet Landscape",
                    width: 1279,
                    height: 768,
                },
                {
                    label: "ZOOM 200%",
                    width: 1440,
                    height: 900,
                },
            ],
        },
    ]
    }
    
}