const componentUrl = "assets/design-system/dist/components/select/examples.html";

module.exports = options => {
    return {
        scenarios: [{
                label: "Select",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            {
                label: "Select - Click",
                url: `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                clickSelector: ".br-select"
            }
            
        ]
    }
}