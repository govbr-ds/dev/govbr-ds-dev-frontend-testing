const componentUrl = "assets/design-system/dist/components/message/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Message",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            // {
            //     "label": "Message - Danger Close",
            //     "url": `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     "clickSelector": ".br-message[danger] .close .br-button"
            // },
            // {
            //     "label": "Message - HoverCloseInformation",
            //     "url": `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     "hoverSelector": ".br-message[info] .close .br-button"
            // }
        ]
    }
}