const componentUrl = "assets/design-system/dist/components/radio/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Radiobutton",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            {
                "label": "Radiobutton - ClickFirst",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "clickSelector": ".br-radio input[type='radio']+label"
            },
            {
                "label": "Radiobutton - HoverFirst",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "hoverSelector": ".br-radio input[type='radio']+label"
            }
        ]
    }
}