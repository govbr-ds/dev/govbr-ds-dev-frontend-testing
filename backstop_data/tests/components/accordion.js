const componentUrl = "assets/design-system/dist/components/accordion/examples.html";

module.exports = (options) => {
  return {
    scenarios: [{
        label: "Accordion",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        selectors: [".br-accordion"],
      },
      {
        label: "Accordion - Hover",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        selectors: [".br-accordion"],
        hoverSelector: ".br-accordion .header .title",
      },
      {
        label: "Accordion - Click",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        selectors: [".br-accordion"],
        delay: options.DEFAULT_DELAY,
        clickSelector: ".br-accordion .header .title",
      },
      {
        label: "Accordion Negative",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        selectors: [".br-accordion[negative]"],
        misMatchThreshold: 0.0001
      },
      {
        label: "Accordion Negative - Click",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        selectors: [".br-accordion[negative]"],
        delay: options.DEFAULT_DELAY,
        clickSelector: ".br-accordion[negative] .header .title",
      },
      {
        label: "Accordion Negative - Hover",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        selectors: [".br-accordion[negative]"],
        hoverSelector: ".br-accordion[negative] .header .title",
      },
    ],
  };
};