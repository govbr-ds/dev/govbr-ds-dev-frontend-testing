const componentUrl =

'assets/design-system/dist/components/notification/examples.html'



module.exports = options => {
    

    
    return {
        
        scenarios: [
            
            {
                
                label: 'Notification',
                
                url: `${options.baseUrl}/${componentUrl}`,
                
                referenceUrl: `${options.refUrl}/${componentUrl}`
                
            },
            
            {
                
                label: 'Notification - ClickMenuFirstItem',
                
                url: `${options.baseUrl}/${componentUrl}`,
                
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                
                clickSelector: ['.br-tab .tab-nav ul .tab-item:first-child'],
                
                postInteractionWait: 2000
                
            },
            
            {
                
                label: 'Notification - ClickMenuLastItem',
                
                url: `${options.baseUrl}/${componentUrl}`,
                
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                
                clickSelector: ['.br-tab .tab-nav ul .tab-item:last-child'],
                
                postInteractionWait: 2000
                
            },
            
            {
                
                label: 'Notification - ClickOnListItem',
                
                url: `${options.baseUrl}/${componentUrl}`,
                
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                
                clickSelector: ['.br-tab, .br-list, button.br-item'],
                
                postInteractionWait: 2000
                
            }
            
        ]
        
    }
    
}