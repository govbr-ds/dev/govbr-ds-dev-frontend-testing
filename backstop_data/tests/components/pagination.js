const componentUrl = "assets/design-system/dist/components/pagination/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Pagination",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            {
                "label": "Pagination - HoverLastChild",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "hoverSelector": ".br-pagination li:last-child"
            },
            {
                "label": "Pagination - ClickLastChild",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "clickSelector": ".br-pagination li:last-child"
            }
        ]
    }
}