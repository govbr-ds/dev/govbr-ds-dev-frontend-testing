const componentUrl = "assets/design-system/dist/components/table/examples.html";

module.exports = options => {
    
    return {
        "scenarios": [
        {
            label: "Table",
            url: `${options.baseUrl}/${componentUrl}`,
            // referenceUrl: `${options.refUrl}/${componentUrl}`,
            // "selectors": [".br-table"],
            // postInteractionWait: 2000,
            // misMatchThreshold : 0.25,
        },
        // {
        //     label: "Table - Click 3th child",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     clickSelector: ".br-table table tr:nth-child(3)",
        //     postInteractionWait: 2000,
        //     misMatchThreshold : 0.25,
        // },
        // {
        //     label: "Table - Seach Bar",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     // referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     "selectors": [".br-table"],
        //     clickSelector: ".search-trigger .br-button",
        //     // postInteractionWait: 2000,
        //     // misMatchThreshold : 0.25,
        // },
        // {
        //     label: "Table - Abrir densidades",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     // referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     "selectors": [".br-table"],
        //     clickSelector: ".actions-trigger > .br-button",
        //     // postInteractionWait: 1000,
        //     // misMatchThreshold : 0.26,
        // },
        // {
        //     label: "Table - Linha selecionada",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     // referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     "selectors": [".br-table"],
        //     clickSelector: "tbody .br-checkbox input",
        //     // postInteractionWait: 1000,
        //     // misMatchThreshold : 0.26,
        // },
        // {
        //     label: "Table - Collapse aberto",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     // referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     "selectors": [".br-table"],
        //     clickSelector: 'tbody [data-toggle="collapse"]',
        //     // postInteractionWait: 1000,
        //     // misMatchThreshold : 0.26,
        // },
        // {
        //     label: "Table - Click Large Grid Button",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     clickSelector: ".top-bar > .grid-button > .grid-large-trigger",
        //     postInteractionWait: 1000,
        //     misMatchThreshold : 0.25,
        // },
        // {
        //     label: "Table - Click on Select Item",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     clickSelector: ".footer > .items > .br-select > .br-input > button",
        //     postInteractionWait: 1000,
        //     misMatchThreshold : 0.25,
        // },
        // {
        //     label: "Table - Click on Select Page",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     clickSelector: ".footer > .pagination > .selectors > .br-select > .br-input > button",
        //     postInteractionWait: 1000,
        //     misMatchThreshold : 0.25,
        // },
        // {
        //     label: "Table - Click on Checkbox-CheckAll",
        //     url: `${options.baseUrl}/${componentUrl}`,
        //     referenceUrl: `${options.refUrl}/${componentUrl}`,
        //     clickSelector: ".br-table > .headers > .syncscroll > .item > .br-checkbox input[type='checkbox'] + label",
        //     postInteractionWait: 2000,
        //     misMatchThreshold : 0.25,
        // }
    ]
    }
}