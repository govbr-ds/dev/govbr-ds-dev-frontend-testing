const componentUrl = "assets/design-system/dist/components/list/examples.html";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: "List",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
      },
      {
        label: "List - Click Collapsible",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        selectors: "viewport",
        scrollToSelector: ".br-list .br-item > .content",
        clickSelector: ".br-list .br-item > .content",
        postInteractionWait: 3000,
      },
      {
        label: "List - HoverCollapsibleList",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,

        scrollToSelector: ".br-list .br-item > .content",
        hoverSelector: ".br-list .br-item > .content",
        selectors: "viewport",
        postInteractionWait: 3000,
      },
    ],
  };
};
