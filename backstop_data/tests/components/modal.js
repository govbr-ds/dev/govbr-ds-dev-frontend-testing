const componentUrl = "assets/design-system/dist/components/modal/examples.html";

module.exports = options => {
    return {
        scenarios: [{
            label: "Modal",
            url: `${options.baseUrl}/${componentUrl}`,
            referenceUrl: `${options.refUrl}/${componentUrl}`,
            hideSelectors: ["*.is-loading, *.loading, *[loading], .rounded"],
        }]
    }
}