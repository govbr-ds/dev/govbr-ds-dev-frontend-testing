const componentUrl = "assets/design-system/dist/components/menu/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Menu",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
            },
            // {
            //     "label": "Menu - Click",
            //     "url": `${options.baseUrl}/${componentUrl}`,
            //     referenceUrl: `${options.refUrl}/${componentUrl}`,
            //     "clickSelector": ".support:last-child"
            // }
        ]
    }
}