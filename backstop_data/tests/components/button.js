const componentUrl = "assets/design-system/dist/components/button/examples.html";

module.exports = options => {
    return {
        "scenarios": [{
                "label": "Button",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "hideSelectors": [
                    ".loading"
                ]
            },
            {
                "label": "Button - Click",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "clickSelector": ".br-button.secondary",
                "hideSelectors": [
                    ".loading"
                ]
            },
            {
                "label": "Button - Hover",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "hoverSelector": ".br-button.secondary",
                "hideSelectors": [
                    ".loading"
                ]
            },
            {
                "label": "Button - ClickCircleButton",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "clickSelector": ".br-button.secondary.circle.small",
                "hideSelectors": [
                    ".loading"
                ]
            },
            {
                "label": "Button - HoverCircleButton",
                "url": `${options.baseUrl}/${componentUrl}`,
                referenceUrl: `${options.refUrl}/${componentUrl}`,
                "clickSelector": ".br-button.secondary.circle.small",
                "hideSelectors": [
                    ".loading"
                ]
            }
        ]
    }
}