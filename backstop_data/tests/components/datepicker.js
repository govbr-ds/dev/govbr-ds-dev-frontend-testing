const componentUrl = "assets/design-system/dist/components/datepicker/examples.html";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: "Datepicker",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
      },
      {
        label: "Datepicker - Click Datepicker",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        postInteractionWait: 1000,
        clickSelector: ".br-input button.icon",
      },
      {
        label: "Datepicker - Click Next Month",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        postInteractionWait: 2000,
        clickSelector: [".br-input button.icon", ".flatpickr-next-month"],
        misMatchThreshold : 0.1,
      },
      {
        label: "Datepicker - Click Prev Month",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        postInteractionWait: 1000,
        clickSelector: [".br-input button.icon", ".flatpickr-prev-month"],
      },
      {
        label: "Datepicker - Click Arrow Down",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        postInteractionWait: 1000,
        clickSelector: [
          ".br-input button.icon",
          ".flatpickr-monthDropdown-months",
        ],
      },
      {
        label: "Datepicker - Timepicker(single)",
        url: `${options.baseUrl}/${componentUrl}`,
        referenceUrl: `${options.refUrl}/${componentUrl}`,
        postInteractionWait: 1000,
        readySelector: "[data-type=time]",
        clickSelector: ["[data-type=time]"],
      },
      {
          label: "Datepicker - Date + Timepicker",
          url: `${options.baseUrl}/${componentUrl}`,
          referenceUrl: `${options.refUrl}/${componentUrl}`,
          postInteractionWait: 1000,
          readySelector: "[data-type]",
          clickSelector: ["[data-type=datetime-local]"]
      },
    ],
  };
};