const componentUrl = "components/step/examples.html";

module.exports = options => {
    return {
        scenarios: [
            {
                label: "Referencias",
                url: `${options.baseUrl}/${componentUrl}`,
            },
            // Vertical - Direita
            {
                label: "Layout-Vertical-Alinhamento-a-Direita",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": [".br-step.vertical"],
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Direita-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": [".br-step.vertical"],
                "hoverSelector": [".br-step.vertical .step-progress-btn:nth-child(2)"],
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Direita-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": [".br-step.vertical"],
                "clickSelector": [".br-step.vertical .step-progress-btn:nth-child(2)"],
                postInteractionWait: 500,
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Direita-hover-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": [".br-step.vertical"],
                "hoverSelector": [".br-step.vertical .step-progress-btn:nth-child(3)"],
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Direita-click-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": [".br-step.vertical"],
                "clickSelector": [".br-step.vertical .step-progress-btn:nth-child(3)"],
                postInteractionWait: 500,
            },
            // Vertical - Esquerda
            {
                label: "Layout-Vertical-Alinhamento-a-Esquerda",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step.vertical[data-label="left"]'],
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Esquerda-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step.vertical[data-label="left"]'],
                "hoverSelector": ['.br-step.vertical[data-label="left"] .step-progress-btn:nth-child(2)'],
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Esquerda-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step.vertical[data-label="left"]'],
                "clickSelector": ['.br-step.vertical[data-label="left"] .step-progress-btn:nth-child(2)'],
                postInteractionWait: 500,
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Esquerda-hover-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step.vertical[data-label="left"]'],
                "hoverSelector": ['.br-step.vertical[data-label="left"] .step-progress-btn:nth-child(3)'],
            },
            {
                label: "Layout-Vertical-Alinhamento-a-Esquerda-click-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step.vertical[data-label="left"]'],
                "clickSelector": ['.br-step.vertical[data-label="left"] .step-progress-btn:nth-child(3)'],
                postInteractionWait: 500,
            },
            // Horizontal - Abaixo
            {
                label: "Layout-Horizontal-Alinhamento-Abaixo",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="bottom"]'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Abaixo-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="bottom"]'],
                "hoverSelector": ['.br-step[data-label="bottom"] .step-progress-btn:nth-child(2)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Abaixo-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="bottom"]'],
                "clickSelector": ['.br-step[data-label="bottom"] .step-progress-btn:nth-child(2)'],
                postInteractionWait: 500,
            },
            {
                label: "Layout-Horizontal-Alinhamento-Abaixo-hover-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="bottom"]'],
                "hoverSelector": ['.br-step[data-label="bottom"] .step-progress-btn:nth-child(3)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Abaixo-click-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="bottom"]'],
                "clickSelector": ['.br-step[data-label="bottom"] .step-progress-btn:nth-child(3)'],
                postInteractionWait: 500,
            },
            // Horizontal - Acima
            {
                label: "Layout-Horizontal-Alinhamento-Acima",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="top"]'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Acima-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="top"]'],
                "hoverSelector": ['.br-step[data-label="top"] .step-progress-btn:nth-child(2)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Acima-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="top"]'],
                "clickSelector": ['.br-step[data-label="top"] .step-progress-btn:nth-child(2)'],
                postInteractionWait: 500,
            },
            {
                label: "Layout-Horizontal-Alinhamento-Acima-hover-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="top"]'],
                "hoverSelector": ['.br-step[data-label="top"] .step-progress-btn:nth-child(3)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Acima-click-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="top"]'],
                "clickSelector": ['.br-step[data-label="top"] .step-progress-btn:nth-child(3)'],
                postInteractionWait: 500,
            },
            // Horizontal - Direita
            {
                label: "Layout-Horizontal-Alinhamento-Direita",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="right"]'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Direita-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="right"]'],
                "hoverSelector": ['.br-step[data-label="right"] .step-progress-btn:nth-child(2)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Direita-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="right"]'],
                "clickSelector": ['.br-step[data-label="right"] .step-progress-btn:nth-child(2)'],
                postInteractionWait: 500,
            },
            {
                label: "Layout-Horizontal-Alinhamento-Direita-hover-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="right"]'],
                "hoverSelector": ['.br-step[data-label="right"] .step-progress-btn:nth-child(3)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Direita-click-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="right"]'],
                "clickSelector": ['.br-step[data-label="right"] .step-progress-btn:nth-child(3)'],
                postInteractionWait: 500,
            },
            // Horizontal - Esquerda
            {
                label: "Layout-Horizontal-Alinhamento-Esquerda",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="left"]'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Esquerda-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="left"]'],
                "hoverSelector": ['.br-step[data-label="left"] .step-progress-btn:nth-child(2)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Esquerda-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="left"]'],
                "clickSelector": ['.br-step[data-label="left"] .step-progress-btn:nth-child(2)'],
                postInteractionWait: 500,
            },
            {
                label: "Layout-Horizontal-Alinhamento-Esquerda-hover-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="left"]'],
                "hoverSelector": ['.br-step[data-label="left"] .step-progress-btn:nth-child(3)'],
            },
            {
                label: "Layout-Horizontal-Alinhamento-Esquerda-click-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.br-step[data-label="left"]'],
                "clickSelector": ['.br-step[data-label="left"] .step-progress-btn:nth-child(3)'],
                postInteractionWait: 500,
            },
            // Horizontal - Icone-Esquerda
            {
                label: "Layout-Horizontal-Icone-Alinhamento-Esquerda",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.ihl .br-step[data-label="left"]'],
            },
            {
                label: "Layout-Horizontal-Icone-Alinhamento-Esquerda-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.ihl .br-step[data-label="left"]'],
                "hoverSelector": ['.ihl .br-step[data-label="left"] .step-progress-btn:nth-child(2)'],
            },
            {
                label: "Layout-Horizontal-Icone-Alinhamento-Esquerda-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.ihl .br-step[data-label="left"]'],
                "clickSelector": ['.ihl .br-step[data-label="left"] .step-progress-btn:nth-child(2)'],
                postInteractionWait: 500,
            },
            {
                label: "Layout-Horizontal-Icone-Alinhamento-Esquerda-hover-2",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.ihl .br-step[data-label="left"]'],
                "hoverSelector": ['.ihl .br-step[data-label="left"] .step-progress-btn:nth-child(3)'],
            },
            // Horizontal - Alerta-Direita
            {
                label: "Layout-Horizontal - Alerta-Direita",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.ahr .br-step[data-label="right"]'],
            },
            {
                label: "Layout-Horizontal - Alerta-Direita-hover",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.ahr .br-step[data-label="right"]'],
                "hoverSelector": ['.ahr .br-step[data-label="right"] .step-progress-btn:nth-child(2)'],
            },
            {
                label: "Layout-Horizontal - Alerta-Direita-click",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.ahr .br-step[data-label="right"]'],
                "clickSelector": ['.ahr .br-step[data-label="right"] .step-progress-btn:nth-child(2)'],
                postInteractionWait: 500,
            },
             // Vazio
             {
                label: "Vazio",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.void .br-step[data-type="void"]'],
            },
             // Vazio - Com alert
             {
                label: "Vazio com Alert",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.avoid .br-step[data-type="void"]'],
            },
             // Simples
             {
                label: "Simples",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.simple .br-step[data-type="simple"]'],
            },
             // Texto
             {
                label: "Texto",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": ['.text .br-step[data-type="text"]'],
            },

        ]
    }
}