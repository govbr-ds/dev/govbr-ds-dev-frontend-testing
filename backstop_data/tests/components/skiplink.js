const componentUrl = "components/skiplink/examples.html";

module.exports = options => {
    return {
        scenarios: [
            {
                label: "Referencias",
                url: `${options.baseUrl}/${componentUrl}`,
            },
            {
                label: "Tipo Simples",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": [".br-skiplink a"],
            },
            {
                label: "Tipo Composto",
                url: `${options.baseUrl}/${componentUrl}`,
                "selectors": [".br-skiplink.full"],
            },
        ]
    }
}