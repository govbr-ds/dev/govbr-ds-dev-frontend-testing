const templateUrl = 'templates/base/examples/base-offcanvas.html'

module.exports = options => {
  return {
    scenarios: [
      {
        label: 'Base Offcanvas',
        selectors: ['.template-base'],
        url: `${options.baseUrl}/${templateUrl}`
      },
      {
        label: 'Base Offcanvas - header',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['header.br-header']
      },
      {
        label: 'Base Offcanvas - footer',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.br-footer'],
        removeSelectors: ['.br-header'],
        scrollToSelector: '.br-footer'
      },
      {
        label: 'Base Offcanvas - menu',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.template-base'],
        clickSelector: '.br-button#navigation',
      },
      {
        label: 'Base Offcanvas - breadcrumb',
        url: `${options.baseUrl}/${templateUrl}`,
        removeSelectors: ['.br-header'],
        selectors: ['.br-breadcrumb']
      }
    ]
  }
}
