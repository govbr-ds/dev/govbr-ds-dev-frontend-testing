const templateUrl = '/assets/design-system/dist/templates/base/base.html'

module.exports = options => {
  return {
    scenarios: [
      {
        label: 'Base',
        url: `${options.baseUrl}/${templateUrl}`
      },
      {
        label: 'Base - header',
        url: `${options.baseUrl}/${templateUrl}`,
        // "referenceUrl": `${options.baseUrl}/assets/design-system/dist/components/header.html`,
        selectors: ['header.br-header']
      },
      {
        label: 'Base - footer',
        url: `${options.baseUrl}/${templateUrl}`,
        // "referenceUrl": `${options.baseUrl}/assets/design-system/dist/components/footer.html`,
        selectors: ['.br-footer'],
        scrollToSelector: '.br-footer'
      },
      {
        label: 'Base - menu',
        url: `${options.baseUrl}/${templateUrl}`,
        // "referenceUrl": `${options.baseUrl}/assets/design-system/dist/components/menu.html`,
        selectors: ['.context-menu .br-menu'],
        scrollToSelector: '.br-menu'
      },
      {
        label: 'Base - breadcrumb',
        url: `${options.baseUrl}/${templateUrl}`,
        // "referenceUrl": `${options.baseUrl}/assets/design-system/dist/components/breadcrumb.html`,
        selectors: ['.br-breadcrumb']
      }
    ]
  }
}
