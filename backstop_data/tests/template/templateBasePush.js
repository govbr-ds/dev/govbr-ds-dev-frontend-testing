const templateUrl = 'templates/base/examples/base-push.html'

module.exports = options => {
  return {
    scenarios: [
      {
        label: 'Base Push',
        selectors: ['.template-base'],
        url: `${options.baseUrl}/${templateUrl}`
      },
      {
        label: 'Base Push - header',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['header.br-header']
      },
      {
        label: 'Base Push - footer',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.br-footer'],
        removeSelectors: ['.br-header'],
        scrollToSelector: '.br-footer'
      },
      {
        label: 'Base Push - menu',
        url: `${options.baseUrl}/${templateUrl}`,
        selectors: ['.template-base'],
        clickSelector: '.br-button#navigation',
      },
      {
        label: 'Base Push - breadcrumb',
        url: `${options.baseUrl}/${templateUrl}`,
        removeSelectors: ['.br-header'],
        selectors: ['.br-breadcrumb']
      }
    ]
  }
}
