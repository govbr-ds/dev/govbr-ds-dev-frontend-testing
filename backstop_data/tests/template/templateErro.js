const templateUrl = 'templates/erro/erro.html'

module.exports = options => {
  
  return {
    scenarios: [
      {
        label: 'Erro',
        url: `${options.baseUrl}/${templateUrl}`,
        delay: 1000,
        selectors: ['body']
      }
    ]
  }
}
