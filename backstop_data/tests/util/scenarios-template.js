module.exports = options => {
  // const templateBase = require('../template/templateBase')(options).scenarios
  const templateBaseOffcanvas = require('../template/templateBaseOffcanvas')(options).scenarios
  const templateBasePush = require('../template/templateBasePush')(options).scenarios
  const templateBaseFluidOffcanvas = require('../template/templateBaseFluidOffcanvas')(options).scenarios
  const templateBaseFluidPush = require('../template/templateBaseFluidPush')(options).scenarios
  const templateErro = require('../template/templateErro')(options).scenarios

  return {
    scenarios: [
      ...templateBaseOffcanvas,
      ...templateBasePush,
      ...templateBaseFluidOffcanvas,
      ...templateBaseFluidPush,
      ...templateErro
    ]
  }
}
