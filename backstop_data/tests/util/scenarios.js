module.exports = options => {
  const accordion = require('../components/accordion')(options).scenarios
  const avatar = require('../components/avatar')(options).scenarios
  const badge = require('../components/badge')(options).scenarios
  const breadcrumb = require('../components/breadcrumb')(options).scenarios
  const button = require('../components/button')(options).scenarios
  const card = require('../components/card')(options).scenarios
  const checkbox = require('../components/checkbox')(options).scenarios
  const datepicker = require('../components/datepicker')(options).scenarios
  const footer = require('../components/footer')(options).scenarios
  const header = require('../components/header')(options).scenarios
  const input = require('../components/input')(options).scenarios
  const item = require('../components/item')(options).scenarios
  const list = require('../components/list')(options).scenarios
  const loading = require('../components/loading')(options).scenarios
  const menu = require('../components/menu')(options).scenarios
  const message = require('../components/message')(options).scenarios
  const modal = require('../components/modal')(options).scenarios
  const notification = require('../components/notification')(options).scenarios
  const onboarding = require('../components/onboarding')(options).scenarios
  const pagination = require('../components/pagination')(options).scenarios
  const radiobutton = require('../components/radiobutton')(options).scenarios
  const scrim = require('../components/scrim')(options).scenarios
  const skiplink = require('../components/skiplink')(options).scenarios
  const step = require('../components/step')(options).scenarios
  const select = require('../components/select')(options).scenarios
  const table = require('../components/table')(options).scenarios
  const tabs = require('../components/tabs')(options).scenarios
  const tag = require('../components/tag')(options).scenarios
  const textarea = require('../components/textarea')(options).scenarios
  const tooltip = require('../components/tooltip')(options).scenarios
  const upload = require('../components/upload')(options).scenarios
  const wizard = require('../components/wizard')(options).scenarios
  const signin = require('../components/signin')(options).scenarios

  return {
    scenarios: [
      ...accordion,
      ...avatar,
      ...breadcrumb,
      ...button,
      ...card,
      ...checkbox,
      // // ...datepicker,
      ...footer,
      ...header,
      ...input,
      ...item,
      ...list,
      ...loading,
      ...menu,
      ...message,
      ...modal,
      ...notification,
      ...pagination,
      ...radiobutton,
      ...scrim,
      ...select,
      ...signin,
      ...skiplink,
      // ...step, 
      ...table,
      ...tabs,
      ...tag,
      ...textarea,
      ...tooltip,
      ...upload,
      ...wizard
    ]
  }
}
