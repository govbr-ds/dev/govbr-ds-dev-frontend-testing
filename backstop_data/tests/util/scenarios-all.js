module.exports = (options) => {
    const all = require("../all/all")(options).scenarios;

    return {
        scenarios: [
            ...all,
        ],
    };
};