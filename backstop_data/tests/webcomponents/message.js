const component = "message";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
      //   {
      //     label: `${component} - Danger Close`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     clickSelector: ".br-message[danger] .close .br-button",
      //   },
      //   {
      //     label: `${component} - HoverCloseInformation`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     hoverSelector: ".br-message[info] .close .br-button",
      //   },
    ],
  };
};
