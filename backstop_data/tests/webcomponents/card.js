const component = "card";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
      },

      // {
      //     "label": `${component} - Click on Expand`,
      //      url: `${options.baseUrl}`,
      //      referenceUrl: `${options.refUrl}`,
      //      onReadyScript: "onReady_wbc.js",
      //     "scrollToSelector": ".br-button",
      //     "clickSelector": "[data-expanded]"
      // }
    ],
  };
};
