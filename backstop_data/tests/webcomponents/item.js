const component = "item";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
        hideSelectors: ["img.rounded"],
      },
      // {
      //   label: `${component}- Click`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   hideSelectors: ["img.rounded"],
      //   clickSelector: ".br-item .row",
      // },
      // {
      //   label: `${component} - Hover`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   hoverSelector: ".br-item .row",
      //   hideSelectors: ["img.rounded"],
      // },
      // {
      //   label: `${component} - Click Checkbox`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   hideSelectors: ["img.rounded"],
      //   readySelector: "[data-toggle=selection]",
      //   clickSelector: ".br-item .br-checkbox",
      // },
      // {
      //   label: `${component} - Click Radio`,
      //   url: `${options.baseUrl}`,
      //   referenceUrl: `${options.refUrl}`,
      //   onReadyScript: "onReady_wbc.js",
      //   hideSelectors: ["img.rounded"],
      //   readySelector: "[data-toggle=selection]",
      //   clickSelector: ".br-item .br-radio",
      // },
    ],
  };
};
