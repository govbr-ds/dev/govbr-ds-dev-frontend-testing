const component = "header";

module.exports = (options) => {
  return {
    scenarios: [
      {
        label: `${component}`,
        url: `${options.baseUrl}`,
        referenceUrl: `${options.refUrl}`,
        onReadyScript: "onReady_wbc.js",
      },
      //   {
      //     label: `${component}- ClickLogin`,
      //     url: `${options.baseUrl}`,
      //     referenceUrl: `${options.refUrl}`,
      //     onReadyScript: "onReady_wbc.js",
      //     postInteractionWait: 1000,
      //     clickSelectors: ".login .br-button",
      //   },
    ],
  };
};
