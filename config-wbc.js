var args = require("minimist")(process.argv.slice(2));
const constants = require("./backstop_data/tests/util/constants.js");

// argumento env irá capturar a URL do ambiente
var environments = {
  local: constants.WBC_URL,
  // dev: constants.DEFAULT_URL,
  // hom: constants.HOM_URL,
  // prod: constants.PROD_URL
};
var default_environment = "local";

if (!args.env) {
  args.env = default_environment;
}
// se for passado um ambiente incorreto, será utilizado o ambiente default
else if (!environments.hasOwnProperty(args.env)) {
  args.env = default_environment;
}

// Site para capturas de tela de referência
if (!args.refHost) {
  args.refHost = environments[args.env];
}

// Site para capturas de tela de teste
if (!args.testHost) {
  args.testHost = environments[args.env];
}

var saveDirectories = {
  bitmaps_reference: "./backstop_data/" + args.env + "_reference",
  // bitmaps_reference: "./backstop_data/reference",
  bitmaps_test: "./backstop_data/" + args.env + "_test",
  html_report: "./backstop_data/" + args.env + "_html_report",
  ci_report: "./backstop_data/" + args.env + "_ci_report",
  engine_scripts: "backstop_data/engine_scripts",
  json_report: "backstop_data/json_report",
};
console.log();
var projectData = require(`./backstop_data/tests/util/scenarios-wbc`)({
  baseUrl: args.testHost,
  //avaliar qual será URL usada para gerar as telas de referência (a baseURL irá ser comparada com essas telas)
  //se não for especificado nenhuma via terminal, será a mesma do ambiente (dev, hom, local, prod)
  refUrl: args.refHost, //mesma URL do ambiente
  // refUrl: constants.HOM_URL, //URL de homologação

  DEFAULT_DELAY: constants.LOCAL_URL,
});

//define o componente a ser testado, passado como parâmetro via terminal
if (process.argv[5]) {
  try {
    projectData = require("./backstop_data/tests/webcomponents/" +
      process.argv[5])({
      baseUrl: args.testHost,
      refUrl: args.refHost,
      //refUrl: constants.HOM_URL,
      // refUrl: constants.DEFAULT_URL,
      DEFAULT_DELAY: constants.DEFAULT_DELAY,
    });
  } catch (e) {
    if (e.code === "MODULE_NOT_FOUND") {
      console.log(
        "VOCÊ DEVE INSERIR CORRETAMENTE O NOME DO COMPONENTE: " +
          process.argv[5]
      );
      process.exit(1);
    }
  }
}

const projectScenarios = projectData.scenarios.map((scenario) => {
  return Object.assign({}, constants.SCENARIO_DEFAULTS, scenario);
});

module.exports = {
  id: "GOVBR-WBC_" + args.env,
  // id: "DS-GOV",
  viewports: constants.VIEWPORTS,
  onBeforeScript: "puppet/onBefore.js",
  //onBeforeScript: "puppet/zoom.js",
  onReadyScript: "puppet/onReady.js",
  scenarios: projectScenarios,
  paths: saveDirectories,
  fileNameTemplate:
    "{configId}_{scenarioLabel}_{selectorLabel}_{viewportLabel}",
  report: ["browser", "CI"],
  engine: "puppetteer",
  engineOptions: {
    browser: "chromium",
    args: ["--no-sandbox"],
    waitTimeout: 300000,
    gotoTimeout: 300000,
    chromeFlags: [
      "--disable-gpu",
      "--force-device-scale-factor=1",
      "--disable-infobars=true",
    ],
  },
  asyncCaptureLimit: 5,
  asyncCompareLimit: 50,
  debug: false,
  debugWindow: false,
  ci: {
    format: "junit",
    testSuiteName: "GOVBR-DS",
  },
  openReport: true,
  verbose: true,
  resembleOutputOptions: {
    errorColor: {
      red: 255,
      green: 0,
      blue: 255,
    },
    //"errorType": "movement",
    transparency: 0.3,
    // "ignoreAntialiasing": true
  },
};
