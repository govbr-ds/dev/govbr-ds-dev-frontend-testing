///<reference types="Cypress" />

import tokens from "../../../fixtures/tokens"

const dsComponenteUrl = "components/tag/examples.html";
const componentClass = ".br-tag";


describe("VALIDATE COMPONENT TAG - Tag de Entrada", () => {
  beforeEach(() => {
    console.log(tokens)
    cy.visit(dsComponenteUrl);
  });

  it("Validate text color", () => {
    cy.get(componentClass)
      .find('span')
      .should("have.color", tokens.colorPure0);
  });

  // it("Validate padding-right on icon", () => {
  //   cy.get(componentClass)
  //     .find('.content [class*="fa-"]')
  //     .should("have.css", "margin-right")
  //     .and("eq", tokens.other.iconPaddingRight);
  // });

  // it("Validate if button close exists when tag is tag-input-close", () => {
  //   cy.get(componentClass).find(".close").parent(".tag-input-close");
  // });

  // it("Validate button close color on tag is-start-close", () => {
  //   cy.get(componentClass)
  //     .find(".close")
  //     .children("button[type=button]")
  //     .should("have.color", tokens.other.btnCloseColor);
  // });

  // it("Validate button close on click to close the tag", () => {
  //   cy.get(componentClass)
  //     .find(".close")
  //     .children("button[type=button]")
  //     .click({
  //       force: true
  //     })
  // });

  // it("Get input field, type a text and press ENTER", () => {
  //   cy.get(".input-tag").type("teste, 10,  {enter}");
  // });

  // it("Validate design tokens", () => {
  //   //REAPROVEITAMENTO DE CÓDIGO
  //   cy.checkDesign(componentClass, tokens.globals);
  // });

  // it("Validate disabled status", () => {
  //   //REAPROVEITAMENTO DE CÓDIGO
  //   cy.disabled(tokens.globals);
  // });

  // context.skip('if your app does not use jQuery', function () {
  //   ['mouseover', 'mouseout', 'mouseenter', 'mouseleave'].forEach((event) => {
  //     it(`dispatches event: '${event}`, function () {
  //       cy.get('.br-tag:first', {
  //         includeShadowDom: true
  //       }).click({
  //         force: true
  //       }).should('have.backgroundColor', '#c5d4eb')

  //     })
  //   })
  // })



});