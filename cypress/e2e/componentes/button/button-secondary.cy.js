///<reference types="Cypress" />

const dsComponenteUrl = "components/button/examples.html";
let componentClass = ".br-button";
const fileData = "button.json";

let tokens;

describe("VALIDATE COMPONENT BUTTON - SECONDARY", () => {

    beforeEach(() => {
        cy.visit(dsComponenteUrl);
        cy.fixture(fileData).then((data) => (tokens = data));
        });

    describe("botão retangular", () => {
        it("Validate design tokens", () => {
            cy.get(componentClass)
                .filter('.secondary')
                .not('.circle')
                .then(($button) => {
                    cy.checkDesign($button, tokens.secondary); //REAPROVEITAMENTO DE CÓDIGO
                });
        });

        it("Validate border radius", () => {
            cy.get(componentClass)
                .filter('.secondary')
                .not('.circle')
                .should("have.css", "border-radius")
                .and("eq", tokens.rectangular.borderRadius);
        });

        it('Validate button design on focus', () => {
            cy.get(componentClass)
                .filter('.secondary')
                .not('.circle')
                .not('.loading')
                .not('[disabled]')
                .each(($btn) => {
                    cy.wrap($btn).click({ release: false, force: true})
                    .focus($btn, tokens.focus); //REAPROVEITAMENTO DE CÓDIGO
                })
        })
    });
    describe("botão circular", () => {
        it("Validate design tokens", () => {
            cy.get(componentClass)
            .filter('.secondary')
            .filter('.circle')
            .then(($button) => {
                cy.checkDesign($button, tokens.secondary); //REAPROVEITAMENTO DE CÓDIGO
            });
        });

        it("Validate border radius", () => {
            cy.get(componentClass)
                .filter('.secondary')
                .filter('.circle')
                .should("have.css", "border-radius")
                .and("eq", tokens.circle.borderRadius);
        });


        it('Validate button design on focus', () => {
            cy.get(componentClass)
                .filter('.secondary')
                .filter('.circle')
                .not('.loading')
                .not('[disabled]')
                .each(($btn) => {
                    cy.wrap($btn).click({ release: false, force: true})
                    .focus($btn, tokens.focus); //REAPROVEITAMENTO DE CÓDIGO
                })
        })
    })
})