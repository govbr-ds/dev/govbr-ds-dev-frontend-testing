///<reference types="Cypress" />

const dsComponenteUrl = "components/button/examples.html";
let componentClass = ".br-button";
const fileData = "button.json";

let tokens;

describe("VALIDATE COMPONENT BUTTON - TERTIARY", () => {

    beforeEach(() => {
        cy.visit(dsComponenteUrl);
        cy.fixture(fileData).then((data) => (tokens = data));
        });
        describe("botão retangular", () => {
            it("Validate design tokens", () => {
                cy.get(componentClass)
                .not('.primary')
                .not('.secondary')
                .not('.circle')
                .then(($button) => {
                    cy.checkDesign($button, tokens.tertiary); //REAPROVEITAMENTO DE CÓDIGO
                });
            })

            it("Validate border radius", () => {
                cy.get(componentClass)
                    .not('.primary')
                    .not('.secondary')
                    .not('.circle')
                    .should("have.css", "border-radius")
                    .and("eq", tokens.rectangular.borderRadius);
            });
            it('Validate button design on focus', () => {
                cy.get(componentClass)
                    .not('.primary')
                    .not('.secondary')
                    .not('.circle')
                    .not('.loading')
                    .not('[disabled]')
                    .each(($btn) => {
                        cy.wrap($btn).click({ release: false, force: true})
                        .focus($btn, tokens.focus); //REAPROVEITAMENTO DE CÓDIGO
                    })
            })

        });
        describe("botão circular", () => {
            it("Validate design tokens", () => {
                cy.get(componentClass)
                .filter('.circle')
                .not('.primary')
                .not('.secondary')
                .then(($button) => {
                    cy.checkDesign($button, tokens.tertiary); //REAPROVEITAMENTO DE CÓDIGO
                });
            });

            it("Validate border radius", () => {
                cy.get(componentClass)
                    .filter('.circle')
                    .not('.primary')
                    .not('.secondary')
                    .should("have.css", "border-radius")
                    .and("eq", tokens.circle.borderRadius);
            });

            it('Validate button design on focus', () => {
                cy.get(componentClass)
                    .filter('.circle')
                    .not('.primary')
                    .not('.secondary')
                    .not('.loading')
                    .not('[disabled]')
                    .each(($btn) => {
                        cy.wrap($btn).click({ release: false, force: true})
                        .focus($btn, tokens.focus); //REAPROVEITAMENTO DE CÓDIGO
                    })
            })

        })

    });