///<reference types="Cypress" />

const dsComponenteUrl = "components/button/examples.html";
let componentClass = ".br-button";
const fileData = "button.json";

let tokens;

describe("VALIDATE COMPONENT BUTTON - DENSITY", () => {

    beforeEach(() => {
        cy.visit(dsComponenteUrl);
        cy.fixture(fileData).then((data) => (tokens = data));
        });

    describe("densidade padrão", () => {
        it("Validate height", () => {
            cy.get(componentClass)
                .not('.large')
                .not('.small')
                .should("have.css", "height")
                .and("eq", tokens.density.mediumHeight);
        });
        it("Validate padding", () => {
            cy.get(componentClass)
                .not('.large')
                .not('.small')
                .should("have.css", "padding")
                .and("eq", tokens.density.mediumPadding);
        });
    })
    describe("densidade baixa", () => {
        it("Validate height", () => {
            cy.get(componentClass)
                .filter('.large')
                .should("have.css", "height")
                .and("eq", tokens.density.largeHeight);
        });
        it("Validate padding", () => {
            cy.get(componentClass)
                .filter('.large')
                .should("have.css", "padding")
                .and("eq", tokens.density.largePadding);
        });
    })
    describe("densidade alta", () => {
        it("Validate height", () => {
            cy.get(componentClass)
                .filter('.small')
                .should("have.css", "height")
                .and("eq", tokens.density.smallHeight);
        });
        it("Validate padding", () => {
            cy.get(componentClass)
                .filter('.small')
                .should("have.css", "padding")
                .and("eq", tokens.density.smallPadding);
        });
    })
})