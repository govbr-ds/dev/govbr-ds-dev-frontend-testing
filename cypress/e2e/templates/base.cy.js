/// <reference types="Cypress" />

const url = Cypress.config('baseUrl');
const dsComponenteUrl = '/templates/base/examples/base-offcanvas.html';
const componentClass = '.br-modal.is-xsmall'
const fileData = 'abas.json';

describe('Validate acessibility a11y', () => {

  before(() => {
    cy.visit(dsComponenteUrl)
    cy.injectAxe()
  })

  it('Validate axe3', () => {

    cy.checkA11y()
  })


})

